import axios from "axios";
import { API_URL } from '../config/config';

const API_ENDPOINT = API_URL + '/v1/policy';

export function getPoliciesList() {
    return axios.get(`${API_ENDPOINT}/list-travel-policies`);
}

export function getPolicyDetails(id) {
    return axios.get(`${API_ENDPOINT}/get-policy-details/${id}`);
}

export function updatePolicyDetails(data) {
    return axios.put(`${API_ENDPOINT}/update-policy-details/1`, data);
}