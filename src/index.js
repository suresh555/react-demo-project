import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './tailwind.output.css';
import PolicyComponent from './pages/policy';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import HeaderComponent from './components/Header';
import LeftNavComponent from './components/LeftNav';

const routing = (
  <Router>
    <div className="relative flex flex-no-wrap">
      <LeftNavComponent />
      <div className="w-full bg-blue-light">
        <HeaderComponent />
        <Route path="/" component={App} />
        <Route path="/" component={PolicyComponent} />
      </div>
    </div>
    
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'))
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
