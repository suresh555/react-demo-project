import React from "react";

class LeftNavComponent extends React.Component {
    render() {
        return (
            <>
                {/* Sidebar starts */}
                <div className="w-72 justify-between ">
                    <div className="px-4 w-60 bg-blue-850 shadow h-screen sm:fixed top-0 fixed z-20">
                        <div className="h-16 w-full flex items-center text-white">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                                <path d="M21.3333 0H2.66667C1.19391 0 0 1.19391 0 2.66667V21.3333C0 22.8061 1.19391 24 2.66667 24H21.3333C22.8061 24 24 22.8061 24 21.3333V2.66667C24 1.19391 22.8061 0 21.3333 0Z" fill="#9DB1D8" />
                                <path d="M7.33325 9.33313C8.43782 9.33313 9.33325 8.4377 9.33325 7.33313C9.33325 6.22856 8.43782 5.33313 7.33325 5.33313C6.22868 5.33313 5.33325 6.22856 5.33325 7.33313C5.33325 8.4377 6.22868 9.33313 7.33325 9.33313Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                                    <path d="M21.3333 0H2.66667C1.19391 0 0 1.19391 0 2.66667V21.3333C0 22.8061 1.19391 24 2.66667 24H21.3333C22.8061 24 24 22.8061 24 21.3333V2.66667C24 1.19391 22.8061 0 21.3333 0Z" fill="#9DB1D8" />
                                </mask>
                                <g mask="url(#mask0)">
                                    <path d="M23.9998 15.9999L17.3332 9.33319L2.6665 23.9999" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                </g>
                            </svg>  CoorporateName
                        </div>
                        <ul className="mt-2">
                            <li className="flex w-full justify-between text-gray-300 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg width={18} height={18} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"  strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M18 6.37943L11.0208 0.713359C9.84924 -0.237786 7.94975 -0.237786 6.77817 0.713359L0 6.21624V18H7V13.1289C7 12.2321 7.89543 11.5052 9 11.5052C10.1046 11.5052 11 12.2321 11 13.1289V18H18V6.37943ZM8.19239 1.86149L2 6.8888V16.3763H5V13.1289C5 11.3354 6.79086 9.88147 9 9.88147C11.2091 9.88147 13 11.3354 13 13.1289V16.3763H16V7.05199L9.6066 1.86149C9.21608 1.54444 8.58291 1.54444 8.19239 1.86149Z" fill="white" fill-opacity="0.76"/>
                                    </svg>
                                    <span className="text-sm  ml-2">Home</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-puzzle" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1" />
                                    </svg>
                                    <span className="text-sm  ml-2">Expenses</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-compass" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="8 16 10 10 16 8 14 14 8 16" />
                                        <circle cx={12} cy={12} r={9} />
                                    </svg>
                                    <span className="text-sm  ml-2">Travel</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-code" width={20} height={20} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="7 8 3 12 7 16" />
                                        <polyline points="17 8 21 12 17 16" />
                                        <line x1={14} y1={4} x2={10} y2={20} />
                                    </svg>
                                    <span className="text-sm  ml-2">Reports</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-puzzle" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1" />
                                    </svg>
                                    <span className="text-sm  ml-2">Lorem Ipsum</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-stack" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="12 4 4 8 12 12 20 8 12 4" />
                                        <polyline points="4 12 12 16 20 12" />
                                        <polyline points="4 16 12 20 20 16" />
                                    </svg>
                                    <span className="text-sm  ml-2">Lorem Ipsum</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-settings" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                        <circle cx={12} cy={12} r={3} />
                                    </svg>
                                    <span className="text-sm  ml-2">Admin</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                {/*Mobile responsive sidebar*/}
                <div className="w-64 z-20 absolute bg-blue-850 shadow shadow  h-screen flex-col justify-between sm:hidden pb-12 transition duration-150 ease-in-out" id="mobile-nav">
                    <div className="px-8">
                        <div className="h-16 w-full flex  text-white items-center">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                                <path d="M21.3333 0H2.66667C1.19391 0 0 1.19391 0 2.66667V21.3333C0 22.8061 1.19391 24 2.66667 24H21.3333C22.8061 24 24 22.8061 24 21.3333V2.66667C24 1.19391 22.8061 0 21.3333 0Z" fill="#9DB1D8" />
                                <path d="M7.33325 9.33313C8.43782 9.33313 9.33325 8.4377 9.33325 7.33313C9.33325 6.22856 8.43782 5.33313 7.33325 5.33313C6.22868 5.33313 5.33325 6.22856 5.33325 7.33313C5.33325 8.4377 6.22868 9.33313 7.33325 9.33313Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                                    <path d="M21.3333 0H2.66667C1.19391 0 0 1.19391 0 2.66667V21.3333C0 22.8061 1.19391 24 2.66667 24H21.3333C22.8061 24 24 22.8061 24 21.3333V2.66667C24 1.19391 22.8061 0 21.3333 0Z" fill="#9DB1D8" />
                                </mask>
                                <g mask="url(#mask0)">
                                    <path d="M23.9998 15.9999L17.3332 9.33319L2.6665 23.9999" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                </g>
                            </svg>  CoorporateName
                        </div>
                        <ul className="mt-2">
                            <li className="flex w-full justify-between text-gray-300 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg width={18} height={18} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"  strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M18 6.37943L11.0208 0.713359C9.84924 -0.237786 7.94975 -0.237786 6.77817 0.713359L0 6.21624V18H7V13.1289C7 12.2321 7.89543 11.5052 9 11.5052C10.1046 11.5052 11 12.2321 11 13.1289V18H18V6.37943ZM8.19239 1.86149L2 6.8888V16.3763H5V13.1289C5 11.3354 6.79086 9.88147 9 9.88147C11.2091 9.88147 13 11.3354 13 13.1289V16.3763H16V7.05199L9.6066 1.86149C9.21608 1.54444 8.58291 1.54444 8.19239 1.86149Z" fill="white" fill-opacity="0.76"/>
                                    </svg>
                                    <span className="text-sm  ml-2">Home</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-puzzle" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1" />
                                    </svg>
                                    <span className="text-sm  ml-2">Expenses</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-compass" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="8 16 10 10 16 8 14 14 8 16" />
                                        <circle cx={12} cy={12} r={9} />
                                    </svg>
                                    <span className="text-sm  ml-2">Travel</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-code" width={20} height={20} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="7 8 3 12 7 16" />
                                        <polyline points="17 8 21 12 17 16" />
                                        <line x1={14} y1={4} x2={10} y2={20} />
                                    </svg>
                                    <span className="text-sm  ml-2">Reports</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-puzzle" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1" />
                                    </svg>
                                    <span className="text-sm  ml-2">Lorem Ipsum</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-stack" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="12 4 4 8 12 12 20 8 12 4" />
                                        <polyline points="4 12 12 16 20 12" />
                                        <polyline points="4 16 12 20 20 16" />
                                    </svg>
                                    <span className="text-sm  ml-2">Lorem Ipsum</span>
                                </div>
                            </li>
                            <li className="flex w-full justify-between text-white-76 hover:text-gray-500 cursor-pointer items-center py-4">
                                <div className="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-settings" width={18} height={18} viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                        <circle cx={12} cy={12} r={3} />
                                    </svg>
                                    <span className="text-sm  ml-2">Admin</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                {/* Sidebar ends */}
            </>
        )
    }
}
export default LeftNavComponent;