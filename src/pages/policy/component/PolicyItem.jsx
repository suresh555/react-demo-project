import { faAngleDown, faAngleUp, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component, Fragment } from 'react';
import { getPolicyDetails } from '../../../services/policy';
import PolicyItemAction from './PolicyItemAction';
import PolicyItemDetails from './PolicyItemDetails';
import VerticleTabs from './VerticleTabs';

class PolicyItem extends Component {
    constructor() {
        super();
        this.state = {
            show: false,
            showAction: false,
            policyDetails: []
        }
    }

    showCollapse = async (id) => {
        this.setState({
            show: !this.state.show
        });
        let res = await getPolicyDetails(id);

        if (res && res.status === 200) {
            this.setState({ policyDetails: res.data });
        }
    }

    showDropdown = () => {
        this.setState({
            showAction: !this.state.showAction
        });
    }

    render() {
        const { showAction, policyDetails } = this.state;
        const { data } = this.props;
        const policies = data.map((item) => {
            return (
                <>
                    <div className="border-white-box rounded-lg bg-white flex items-center justify-between rounded-b-none relative" id="headingOne">
                        <div>
                            <h3 className="font-bold inline px-10 py-6" type="button">
                                {item.name}
                            </h3>
                            <span className="bg-gray-100 text-gray-900 text-xs mx-4 px-8 font-semibold py-1 rounded-full">Default</span>
                        </div>
                        <div>
                            <div className="float-right px-10 py-6 border-l  cursor-pointer">
                                <FontAwesomeIcon onClick={this.showDropdown} icon={faEllipsisV} />
                            </div>
                            <PolicyItemAction showAction={showAction} />
                            <div className="float-right px-10 py-6 cursor-pointer" onClick={() => { this.showCollapse(item.id) }}>
                                {this.state.show ? (<FontAwesomeIcon icon={faAngleUp} />) : <FontAwesomeIcon icon={faAngleDown} />}
                            </div>
                        </div>
                    </div>
                    <div style={{ display: this.state.show ? 'block' : 'none' }} className="border-white-box bg-white border-b-0">
                        <div className="grid grid-cols-6">
                            <VerticleTabs />
                            <div className="col-span-5 ...">
                                <PolicyItemDetails policyDetails={policyDetails} />
                            </div>
                        </div>
                    </div>
                </>
            )
        })
        return (
            <Fragment>
                <div className="container mx-auto py-10 md:w-4/5 w-11/12 px-6">
                    <div className="w-full h-full">
                        <div className="w-full my-4">
                            <div className="rounded-lg">
                                {policies}
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default PolicyItem;
