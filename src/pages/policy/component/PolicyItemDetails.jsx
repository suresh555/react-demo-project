import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';
import EditPolicyDetails from './EditPolicyDetails';

class PolicyItemDetails extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
        }
    }

    showDrawer = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }

    render() {
        const { policyDetails } = this.props;
        return (
            <> 
                <div style={{ display: this.state.showModal ? 'block' : 'none' }}>
                    <EditPolicyDetails toggleDrawer={this.showDrawer}/>
                </div>
                <div className=" mx-auto">
                    <div className="w-full h-full text-sm ">
                        <div className="py-5 px-7 active-bg-tab">
                            <div>
                                <h3 className="font-semibold leading-6 inline">General Policy</h3>
                                <img src="./edit_icon.svg" onClick={this.showDrawer}  className="float-right cursor-pointer" alt="Edit icon"/>
                            </div>
                            <div className="grid grid-cols-2 py-6 pb-2">
                                <div className="">
                                    <p className="text-gray-light">Hotel Class</p>
                                    <p className="font-semibold align-middle inline">
                                        Maximum stars
                                    </p>
                                    <div className="mx-2 ml-5 align-middle inline">
                                        <StarRatings
                                            numberOfStars={5}
                                            rating={4}
                                            starRatedColor="#FFDC39"
                                            starDimension="15px"
                                            starSpacing="1px"
                                        />
                                    </div>
                                </div>
                                <div className="grid grid-cols-1">
                                    <div>
                                        <p className="text-gray-light mb-1">Distance from search location</p>
                                        <p className="font-semibold text-sm">{ policyDetails.distanceFromLocation + " km" }</p>
                                    </div>
                                </div>
                            </div>
                            <div className="grid grid-cols-2 py-4 pb-0">
                                <div className="">
                                    <p className="text-gray-light mb-1">Booking Deadline</p>
                                    <p className="font-semibold inline text-sm ">
                                        {/* More than 7 days in advance of check-in date */}
                                        {policyDetails.deadline+ " days"}
                                    </p>
                                </div>
                                <div className="grid grid-cols-1">
                                    <div>
                                        <p className="text-gray-light mb-1">Budget Flexibility</p>
                                        <p className="font-semibold text-sm ">{ policyDetails.budgetFlexibility+ "%" }</p>
                                    </div>
                                </div>
                            </div>
                            <div className="grid grid-cols-2 py-4 pb-0">
                                <div className="">
                                    <p className="text-gray-light mb-1">Global budget per room per night</p>
                                    <div className="mb-1 flex items-center justify-between mw_210 font-semibold">
                                       
                                            All destinations
                                       
                                        <div className="mx-2 ">
                                                €{policyDetails.budgetAllDesitination}
                                        </div>
                                    </div>
                                </div>
                                <div className="grid grid-cols-1">
                                    <div>
                                        <p className="text-gray-light mb-1">Warning message in expenses</p>
                                        <p className="font-semibold text-sm ">{policyDetails.warningMessage}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="grid grid-cols-2 py-4 pb-0">
                                <div className="">
                                    <p className="text-gray-light mb-1">Budget per city per room per night</p>
                                    <p className="font-semibold inline text-sm " >
                                        <div className="mb-1 flex items-center justify-between mw_210">
                                        Jaipur <span>€100</span>
                                        </div>
                                        <div className="mb-1 flex items-center justify-between mw_210">
                                        Ratlam <span>€100</span>
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="px-7">
                            <div className="flex justify-between">
                                <h3 className="font-semibold leading-10">Approval & Notification</h3>
                                <img src="./edit_icon.svg" onClick={this.showDrawer}  className="float-right cursor-pointer" alt="Edit icon"/>
                            </div>
                            <h3 className="font-semibold leading-10">Approval</h3>
                            <div className="py-4">
                                <div className="">
                                    <p className="text-gray-light">Select when managers should approve hotel bookings. Travel will be booked automatically upon arrival</p>
                                    <div className="py-4">
                                        <input className="align-middle h-4 w-4" type="checkbox" id="check1" name="check1" checked/>
                                        <label className="align-middle px-4 font-semibold" for="check1"> Out of Policy: if the budget is exceeded</label>
                                    </div>
                                </div>
                            </div>
                            <h3 className="font-semibold  leading-10">Notifications</h3>
                            <div className="py-2">
                                <div className="">
                                    <p className="text-gray-light">Select when managers should be notified about hotel bookings. this will not block travelers from booking</p>
                                    <div className="py-4">
                                        <input className="align-middle h-4 w-4" type="checkbox" id="check1" name="check1" checked/>
                                        <label className="align-middle px-4 font-semibold" for="check1"> All Bookings</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default PolicyItemDetails;