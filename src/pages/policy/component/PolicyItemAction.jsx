import React, { Component } from 'react';
import { faBan, faCopy, faFileExport, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PolicyItemAction extends Component {
    state = {}
    render() {
        return (
            <>
                <div
                    style={{ display: this.props.showAction ? 'block' : 'none' }}  
                    className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5" 
                    role="menu" aria-orientation="vertical" 
                    aria-labelledby="user-menu"
                >
                    <a href="#" className="font-semibold block border-b px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                        <FontAwesomeIcon icon={faPencilAlt} />
                        <span className="px-4">Edit</span>
                    </a>
                    <a href="#" className="font-semibold block border-b px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                        <FontAwesomeIcon icon={faCopy} />
                        <span className="px-4">Duplicate</span>
                    </a>
                    <a href="#" className="font-semibold block border-b px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                        <FontAwesomeIcon icon={faFileExport} />
                        <span className="px-4">Export as PDF</span>
                    </a>
                    <a href="#" className="font-semibold block border-b px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                        <FontAwesomeIcon icon={faBan} />
                        <span className="px-4">Disactivate</span>
                    </a>
                    <a href="#" className="font-semibold block px-4 py-2 text-sm text-red-new hover:bg-gray-100" role="menuitem">
                        <FontAwesomeIcon icon={faTrash} />
                        <span className="px-4">Delete</span>
                    </a>
                </div>
            </>
        );
    }
}

export default PolicyItemAction;