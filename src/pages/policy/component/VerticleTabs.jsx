// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCar, faHotel, faPlane, faSuitcase, faTrain } from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react';

class VerticleTabs extends Component {
    state = {}
    render() {
        return (
            <>
                <div className="antialiased min-h-screen">
                    <div className="flex">
                        <nav id="nav" className="w-56 relative">
                            <span className="absolute h-17 w-full active-bg-tab ease-out transition-transform transition-medium"></span>
                            <ul className="relative">
                                <li>
                                    <button type="button" className="py-3_5 px-3 w-full flex items-center focus:outline-none focus-visible:underline border-r-3 active-border-blue">
                                        <img src="./hotel_icon.svg" alt=""/>
                                        <span className="ml-2 text-sm font-medium transition-all ease-out transition-medium tab-blue-font">
                                            Hotels
                                        </span>
                                    </button>
                                </li>
                                <li>
                                    <button type="button" className="py-3_5 px-3 w-full flex items-center focus:outline-none focus-visible:underline">
                                        {/* <FontAwesomeIcon icon={faPlane} /> */}
                                        <img src="./flight_icon.svg" alt="flight_icon"/>
                                        <span className="ml-2 text-sm font-medium transition-all ease-out transition-medium tab-blue-font">
                                            Flights
                                        </span>
                                    </button>
                                </li>
                                <li>
                                    <button type="button" className="py-3_5 px-3 w-full flex items-center focus:outline-none focus-visible:underline">
                                        {/* <FontAwesomeIcon icon={faTrain} /> */}
                                        <img src="./train_icon.svg" alt="train_icon"/>
                                        <span className="ml-2 text-sm font-medium transition-all ease-out transition-medium tab-blue-font">
                                            Trains
                                        </span>
                                    </button>
                                </li>
                                <li>
                                    <button type="button" className="py-3_5 px-3 w-full flex items-center focus:outline-none focus-visible:underline">
                                        {/* <FontAwesomeIcon icon={faCar} /> */}
                                        <img src="./car_icon.svg" alt="car_icon"/>
                                        <span className="ml-2 text-sm font-medium transition-all ease-out transition-medium tab-blue-font">
                                            Cars
                                        </span>
                                    </button>
                                </li>
                                <li>
                                    <button type="button" className="py-3_5 px-3 w-full flex items-center focus:outline-none focus-visible:underline">
                                        {/* <FontAwesomeIcon icon={faSuitcase} /> */}
                                        <img src="./pedieme_icon.svg" alt="pedieme_icon"/>
                                        <span className="ml-2 text-sm font-medium transition-all ease-out transition-medium tab-blue-font">
                                            Perdieme
                                        </span>
                                    </button>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </>
        );
    }
}

export default VerticleTabs;
