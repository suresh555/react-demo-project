import { useForm, Controller } from "react-hook-form";
import Select from "react-select";
import { Input as AntdInput } from "antd";
import { updatePolicyDetails } from "../../../services/policy";

const options = [
  { value: "1", label: "1 day" },
  { value: "2", label: "2 days" },
  { value: "3", label: "3 days" }
];

const distance = [
  { value: "10", label: "10 km" },
  { value: "20", label: "20 km" },
  { value: "30", label: "30 km" }
];

const budgetflex = [
  { value: "10", label: "10%" },
  { value: "20", label: "20%" },
  { value: "30", label: "30%" }
];

const currency = [
  { value: "euro", label: "Euro" },
  { value: "rs", label: "Rupee" },
];
function EditPolicyDetails(props) {
  const { handleSubmit, control, errors } = useForm();
  

  const onSubmit = async data => {
    let policyData = {
      type: '1',
      star_rating: data.rating,
      deadline: data.reactSelect.value,
      budget: data.budget,
      budget_exception_city: data.city,
      distance_from_location: data.distance.value,
      budget_flexibility: data.flexibility.value,
      warning_message: data.message,
      budget_all_desitination: data.destination
    }

    try {
      let res = await updatePolicyDetails(policyData);
      if (res && res.status === 200) {
        console.log(res);
        alert("Policy updated successfully.");
        window.location.reload();
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    
    <aside className="shadow fixed top-0 right-0 z-20 xlg-m-w-768">
      <div className="flex items-center justify-between rounded-tl-xl bg-blue-850 text-white h-20 px-14 text-1_1xl font-semibold">
        <h2>
          Worldwide Travel Policy
        </h2>
        <a href="#" className="cursor-pointer">
          <img src="./close.svg" alt="Close" onClick ={props.toggleDrawer}/>
        </a>
      </div>
      <div className="px-16 py-7 bg-white overflow-y-auto h-calc-drawer rounded-bl-xl">
        <div className="py-2 tab-blue-font text-lg font-semibold mb-5">
          General Policies
        </div>
        <div className="active-bg-tab">
          <div className=" flex items-center px-4 py-3 border-b-1 light-border-blue">
            <img src="./hotel_icon.svg" alt="" />
            <span className="ml-2 text-sm font-medium tab-blue-font">
              Hotels
            </span>
          </div>
          <div className=" px-16">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between relative">
                <div>
                  <label className="font-semibold tab-blue-font">Hotel class</label>
                </div>
                <div>
                  <Controller
                    className="border input-light-border-blue bg-transparent h-9 rounded-md w-25"
                    as={AntdInput}
                    name="rating"
                    control={control}
                    type="number"
                    defaultValue=""
                    rules={{ required: true }}
                  />
                  {errors.rating && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
                </div>
              </div>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between">
                <div>
                  <label className="font-semibold tab-blue-font mb-1 block">Booking deadline</label>
                  <p className="text-gray-light text-sm">Traveller have to book in advance of check-in date</p>
                </div>
                <div className="relative">
                  <Controller className="input-light-border-blue w-25"
                    name="reactSelect"
                    as={Select}
                    options={options}
                    control={control}
                    defaultValue={{ value: "1", label: "1" }}
                    rules={{ required: true }}
                  />
                  {errors.reactSelect && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
                </div>
              </div>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between">
                <div>
                  <label className="font-semibold tab-blue-font mb-1 block">Budget per room per night</label>
                  <p className="text-gray-light text-sm">All destination</p>
                </div>
                <div className="flex items-center justify-between">
                  <Controller
                    as={AntdInput}
                    name="destination"
                    control={control}
                    type="number"
                    defaultValue=""
                    rules={{ required: true }}
                    className="border input-light-border-blue bg-transparent h-9 w-25 rounded-md mr-3"
                  />
                  {errors.destination && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
                  <Select 
                    className="reactSelect input-light-border-blue w-25"
                    name="filters"
                    placeholder="Filters"
                    // defaultValue="Euro"
                    options={currency}
                    defaultInputValue="Euro"
                    isDisabled={true}
                  />
                </div>
              </div>
              <div className="py-6 border-b-1 light-border-blue">
                <p className="text-gray-light text-sm mb-2">Add exceptions for specific destinations (cities)</p>
                <div className="flex items-center justify-start">
                  <div className="relative">
                    <p className="tab-blue-font mb-2 text-sm">City</p>
                    <Controller className=""
                      as={AntdInput}
                      name="city"
                      control={control}
                      defaultValue=""
                      rules={{ required: true }}
                      className="border input-light-border-blue bg-transparent mr-3 h-9 rounded-md"
                    />
                    {errors.city && <span className="absolute text-red-500 text-xs right-3 top-2">*</span>}
                  </div>
                  <div className="relative">
                    <p className="tab-blue-font mb-2 text-sm">Budget per night</p>
                    <Controller className=""
                      as={AntdInput}
                      name="budget"
                      control={control}
                      defaultValue=""
                      type="number"
                      rules={{ required: true }}
                      className="border input-light-border-blue bg-transparent h-9 rounded-md"
                    />
                    {errors.budget && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
                  </div>
                </div>
              </div>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between relative">
                <label className="font-semibold tab-blue-font">Distance Max from search location</label>
                <Controller className="input-light-border-blue w-25"
                  name="distance"
                  as={Select}
                  options={distance}
                  control={control}
                  defaultValue=""
                  rules={{ required: true }}
                />
                {errors.distance && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
              </div>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between relative">
                <label className="font-semibold tab-blue-font">Budget flexibility</label>
                <Controller  className="input-light-border-blue w-25"
                  name="flexibility"
                  as={Select}
                  options={budgetflex}
                  control={control}
                  defaultValue=""
                  rules={{ required: true }}
                />
                {errors.flexibility && <span className="absolute text-red-500 text-xs right-0 top-2">*</span>}
              </div>
              <div className="py-6">
                <label className="font-semibold tab-blue-font mb-2 block">Worning message in expenses</label>
                <p className="text-gray-light text-sm mb-3">Show warning in expense, with message:</p>
                <Controller
                  as={AntdInput}
                  name="message"
                  control={control}
                  defaultValue=""
                  placeholder="Message"
                  rules={{ required: true }}
                  className="txtarea-bg-dark-blue px-2 py-2 w-full text-sm rounded-md min-h-76"
                />
              </div>
              <div className="py-6 border-b-1 light-border-blue flex items-center justify-between">
                <a href="javascript:void(0)" className="h-10 font-semibold tab-blue-font px-16 text-sm items-center flex font-semibold cursor-pointer" onClick ={props.toggleDrawer}>Cancel</a>
                <button className="font-bold bg-blue-850 h-10 text-white px-16 text-sm rounded-md cursor-pointer" type="submit">Save Travel Policy</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </aside>
    
  );
}

export default EditPolicyDetails;
