import React from "react";
import PolicyItem from "./component/PolicyItem";
import { getPoliciesList } from "../../services/policy";
class PolicyComponent extends React.Component {
    constructor() {
      super();
      this.state = {
        data: []
      }
    }

    componentDidMount = async() => {
      let res = await getPoliciesList();
      console.log(res);
      if (res && res.status === 200) {
        this.setState({ data: res.data.data });
      }
    }

    render() {
      const { data } = this.state;
      return (
        <>
          <PolicyItem data={data} />
        </>
      );
    }
}
export default PolicyComponent;